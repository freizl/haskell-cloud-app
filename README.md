# haskell-cloud-app

A demonstration of how to easily set up a cloud-based web application
using Haskell, Terraform, Nix, and Docker.

See [Deploying Haskell applications with ECS, Docker, and Nix](https://williamyaoh.com/posts/2019-04-09-deploying-haskell-with-ecs-and-nix.html).
