{ mkDerivation, aeson, base, hpack, servant-server, stdenv, warp
, wreq
}:

mkDerivation {
  pname = "haskell-cloud-app";
  version = "0.1.0.0";
  src = ./.;
  isExecutable = true;
  executableHaskellDepends = [ aeson base servant-server warp wreq ];
  executableToolDepends = [ hpack ];
  preConfigure = "hpack";
  license = stdenv.lib.licenses.bsd3;
}
