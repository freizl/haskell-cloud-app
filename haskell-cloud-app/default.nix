{ pkgs ? import <nixpkgs> {} }:

with pkgs;

let callPackage = lib.callPackageWith haskell.packages.ghc822;
in callPackage ./haskell-cloud-app.nix { inherit stdenv; }
