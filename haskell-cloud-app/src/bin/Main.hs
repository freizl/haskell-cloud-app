-- Main.hs

{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE DeriveAnyClass    #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators     #-}

module Main where

import Control.Monad.IO.Class
import Data.Aeson
import GHC.Generics
import Network.Wai.Handler.Warp
import Network.Wreq hiding ( Proxy )
import Servant

type WordsAPI = "words"
             :> ReqBody '[PlainText] String
             :> Post '[JSON] Words

type HealthCheck = "internal" :> "health" :> Get '[JSON] NoContent

type PingAPI = "ping" :> Post '[JSON] NoContent

type API = WordsAPI :<|> PingAPI :<|> HealthCheck

data Words = Words { words :: [String] }
  deriving (Eq, Show, Generic, ToJSON)

server :: Proxy API
server = Proxy

handler :: Server API
handler = wordsHandler :<|> pingHandler :<|> healthCheckHandler
  where wordsHandler :: String -> Handler Words
        wordsHandler text = pure $
          Words { Main.words = Prelude.words text }

        pingHandler :: Handler NoContent
        pingHandler = do
          _ <- liftIO $ post "<insert https url here>" ([] :: [Part])
          pure NoContent

        healthCheckHandler :: Handler NoContent
        healthCheckHandler = pure NoContent

main :: IO ()
main = runSettings appSettings $ serve server handler
  where appSettings = setPort 8000 $ setHost "0.0.0.0" $ defaultSettings
